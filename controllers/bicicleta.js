var Bicicleta =  require('../models/bicicleta');


exports.bicicletaList =  function(req, res){

    res.render('bicicletas/index', {bicis: Bicicleta.allBicis});
}

exports.bicicletaCreateGet =  function(req, res){

    res.render('bicicletas/create');
}

exports.bicicletaCreatePost =  function(req, res){

    var bici =  new Bicicleta(req.body.id, req.body.color, req.body.modelo);
    bici.ubicacion= [req.body.ltd, req.body.lng];
    Bicicleta.add(bici);

    res.redirect('/bicicletas');
}
exports.bicicletaDeletePost= function(req, res){

        Bicicleta.removeById(req.body.id);
        res.redirect('/bicicletas');


}