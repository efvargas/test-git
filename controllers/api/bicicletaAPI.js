var Bicicleta = require('../../models/bicicleta');
const { bicicletaDeletePost } = require('../bicicleta');

exports.bicicleta_list = function(req, res){


    Bicicleta.allBicis(function(err, bicis){

        res.status(200).json({
            bicicletas: bicis
        });
    });

    
}

exports.bicicleta_create =  function(req, res){

    var bici = Bicicleta.createInstance(req.body.code, req.body.color, req.body.modelo, [req.body.lat,req.body.lng])
    
    Bicicleta.add(bici, function(err, newBike){
        if (err) console.log(err);});
   
    res.status(200).json({bicicleta: bici});
}

exports.bicicleta_update = function(req, res){
      Bicicleta.findByCode(req.body.code, function(err, bici){

        bici.code = req.body.code;
        bici.color = req.body.color;
        bici.modelo = req.body.modelo;
        bici.ubicacion = [req.body.lat,req.body.lng];
        Bicicleta.updateBici( bici); 

        res.status(200).json({bicicleta: bici});

     });

 
}

exports.bicicleta_delete= function(req, res){

    Bicicleta.removeByCode(req.body.code);
    res.status(204).send();
    


}