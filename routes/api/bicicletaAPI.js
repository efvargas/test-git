var express =  require('express');
var router = express.Router();
var biciController =  require('../../controllers/api/bicicletaAPI');

router.get("/", biciController.bicicleta_list);
router.post("/create", biciController.bicicleta_create);
router.post("/update", biciController.bicicleta_update);
router.delete("/delete", biciController.bicicleta_delete);




module.exports = router;