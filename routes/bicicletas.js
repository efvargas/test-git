var express =  require('express');
var router = express.Router();
var biciController =  require('../controllers/bicicleta');

router.get('/', biciController.bicicletaList);
router.get('/create', biciController.bicicletaCreateGet);
router.post('/create', biciController.bicicletaCreatePost);
router.post('/:id/delete', biciController.bicicletaDeletePost);


module.exports =  router;   