var mongoose  = require('mongoose');
var Schema = mongoose.Schema;

var bicicletaSchema = new Schema({
    code: Number,
    color: String,
    modelo: String,
    ubicacion: {

        type: [Number], index: { type: "2dsphere", sparse: true }
    }
});

bicicletaSchema.statics.createInstance = function (code, color, modelo, ubicacion) {
    return new this({
        code: code,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    });
}

bicicletaSchema.methods.toString = function () {
    return "codigo: " + this.code + " | color: " + this.color;
}

bicicletaSchema.statics.allBicis = function(cb){
    return this.find({}, cb);
};

bicicletaSchema.statics.add = function(bici, cb){
    this.create(bici, cb);
};

bicicletaSchema.statics.updateBici = function(bici, cb){

    bici.save(bici, cb);
};

bicicletaSchema.statics.findByCode = function(code, cb){
    return this.findOne({ code: code }, cb);
};

bicicletaSchema.statics.removeByCode = function(code, cb){
    return this.deleteOne({ code: code }, cb);
};
module.exports = mongoose.model("Bicicleta", bicicletaSchema);



/* var Bicicleta = function(id, color, modelo, ubicacion){
    this.id = id;
    this.modelo =modelo;
    this.color= color;
    this.ubicacion=ubicacion;

    
} */

/* Bicicleta.prototype.toString = function(){

    return "id: " + this.id + " | color: " + this.color;
}

Bicicleta.allBicis = [];
Bicicleta.add = function(aBici){

    Bicicleta.allBicis.push(aBici);
}

Bicicleta.findById = function(aBiciId){

    var b= Bicicleta.allBicis.find(x => x.id == aBiciId);

    if (b) {
        return b;
    } else {
        throw new Error(" no existe una bicix con id " + aBiciId);
    }
}

Bicicleta.removeById = function(aBiciId){

    var b= Bicicleta.allBicis.find(x => x.id == aBiciId);

    for (let index = 0; index < Bicicleta.allBicis.length; index++) {
        
        if (Bicicleta.allBicis[index].id == aBiciId) {
            Bicicleta.allBicis.splice(index, 1);
            break;
        }
        
    }
    if (b) {
        return b;
    } else {
        throw new Error(" no existe una bici con id " + aBiciId);
    }
}

var a = new Bicicleta(1,'rojo', 'urbana', [4.6552131,-74.0792192]);
var b = new Bicicleta(2,'blanca', 'urbana', [6.2452124,-75.6511237]);
Bicicleta.add(a);
Bicicleta.add(b);

module.exports =  Bicicleta; */