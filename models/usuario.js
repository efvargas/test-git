var mongoose = require("mongoose");
var Reserva = require("./reserva");
const bcrypt = require("bcrypt");
const uniqueValidator = require("mongoose-unique-validator");

const Token = require("../models/token");
const mailer = require("../mailer/mailer");
const crypto = require("crypto");

const saltRounds = 10;
var Schema = mongoose.Schema;

var validateEmail = function(email){
    const re = /^\w([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(email);
};

var usuarioSchema = new Schema({
    nombre: { 
        type: String,
        trim: true,
        required: [true, "El nombre es obligatorio"]
    },
    email: {
        type: String,
        trim: true,
        required: [true, "El email es obligatorio"],
        lowercase: true,
        unique: true,
        validate: [validateEmail, "Ingrese un email valido"],
        match: [/^\w([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/]
    },
    password: {
        type: String,
        required: [true, "El password es obligatorio"]
    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verificado: {
        type: Boolean,
        default: false
    },
    googleId: String,
    facebookId: String
});

usuarioSchema.plugin(uniqueValidator, { message: `El {PATH} ya existe con otro usuario`});

usuarioSchema.pre("save", function(next){
    if (this.isModified("password")){
        this.password = bcrypt.hashSync(this.password, saltRounds);
    }
    next();
});

usuarioSchema.methods.validPassword = function(password){
    return bcrypt.compareSync(password, this.password);
}

usuarioSchema.methods.resetPassword = function (cb) {
    const token = new Token({ _userId: this.id, token: crypto.randomBytes(16).toString("hex") });
    const email_destination = this.email;

    token.save(function(err){
        if (err) { return cb(err); }

        const mailOptions = {
            from: "no-reply@redbicicletas.com",
            to: email_destination,
            subject: "Reseteo password de cuenta",
            text: "Hello, \n\n Please click on the following link to reset your pass : http://localhost:5000/token/resetPassword/" + token.token + ".\n"
        };

        mailer.sendMail(mailOptions, function(err){
            if (err) { cb(err); }

            console.log("A verification mail has been sent to  " + email_destination);
        });

        cb(null);
    });
}

usuarioSchema.methods.reservar = function(codigoBici, desde, hasta, cb){
    var reserva = new Reserva({ usuario: this._id, bicicleta: codigoBici, desde: desde, hasta: hasta });
    console.log(reserva);
    reserva.save(cb);
};

usuarioSchema.methods.enviar_email_bienvenida = function(cb){
    const token = new Token({ _userId: this.id, token: crypto.randomBytes(16).toString("hex") });
    const email_destination = this.email;

    token.save(function(err){
        if (err) { return console.log(err.message); }

        const mailOptions = {
            from: "no-reply@redbicicletas.com",
            to: email_destination,
            subject: "Verificación de cuenta",
            text: "Hola, \n\n Por favor verificar su cuenta haga click en este link: http://localhost:3000/token/confirmation/" + token.token + ".\n"
        };

        mailer.sendMail(mailOptions, function(err){
            if (err) { console.log(err.message); }

            console.log("La varificación del email ha sido enviada a " + email_destination);
        });
    });
};

usuarioSchema.statics.findOneOrCreateByGoogle = function findOneOrCreate(condition, callback) {
    const self = this;
    console.log(condition);
    self.findOne({
            $or: [
                { "googleId": condition.id }, 
                { "email": condition.emails[0].value }
            ]
        },
        (err, result) => {
            if (result) {
                callback(err, result);
            }
            else {
                console.log("**** CONDITION ****");
                console.log(condition);
                let values = {
                    "googleId": condition.id,
                    "email": condition.emails[0].value,
                    "nombre": condition.displayName || "SIN NOMBRE",
                    "verificado": true,
                    "password": condition._json.etag
                };
                self.create(values, (err, result) => {
                    if (err) { console.log(err); }

                    return callback(err, result);
                });
            }
        }
    );
}

usuarioSchema.statics.findOneOrCreateByFacebook = function findOneOrCreate(condition, callback) {
    const self = this;
    console.log(condition);
    self.findOne({
            $or: [
                { "facebookId": condition.id }, 
                { "email": condition.emails[0].value }
            ]
        },
        (err, result) => {
            if (result) {
                callback(err, result);
            }
            else {
                console.log("**** CONDITION ****");
                console.log(condition);
                let values = {
                    "facebookId": condition.id,
                    "email": condition.emails[0].value,
                    "nombre": condition.displayName || "SIN NOMBRE",
                    "verificado": true,
                    "password": crypto.randomBytes(16).toString("hex")
                };
                self.create(values, (err, result) => {
                    if (err) { console.log(err); }

                    return callback(err, result);
                });
            }
        }
    );
}

module.exports = mongoose.model("Usuario", usuarioSchema);