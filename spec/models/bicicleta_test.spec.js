var Bicicleta =  require ("../../models/bicicleta")
var mongoose = require("mongoose");


describe("Testing Bicicletas", function(){
    
    beforeEach(function(done){
        var mongoDB = "mongodb://localhost/testdb";
         mongoose.connect(mongoDB, { useUnifiedTopology: true, useNewUrlParser: true, useCreateIndex: true});


        const db = mongoose.connection;
        db.on("error", console.error.bind(console, "Connection error!"));
        db.on("open", function(){
            console.log("We are connected to test database!");
            done();
        });
    });

    afterEach(function(done){

     
         Bicicleta.deleteMany({}, function(err, success){
            if (err) console.log(err);
            mongoose.connection.close();
            done();
        }); 
    });

    describe("Bicicleta.createInstance", () => {
        it("Create instance", () => {
            console.log("CREATING...");
            var b = Bicicleta.createInstance(1, "verde", "urbana", [-34.5, -54.1]);

            expect(b.code).toBe(1);
            expect(b.color).toBe("verde");
            expect(b.modelo).toBe("urbana");
            expect(b.ubicacion[0]).toBe(-34.5);
            expect(b.ubicacion[1]).toBe(-54.1);
        });
    });

    describe('Bicicleta.allBicis', () => {
        it('begins empty', (done) => {
            Bicicleta.allBicis(function(err, b){
                expect(b.length).toBe(0);
                done();
            });
        });
    });

    describe("Bicicleta.add", () => {
        it("add bike", (done) => {
            var b = new Bicicleta({ code: 1, color: "verde", modelo: "urbana" });
            Bicicleta.add(b, function(err, newBike){
                if (err) console.log(err);
                Bicicleta.allBicis(function(err, bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(b.code);
                    done();
                });
            });
        });
    });

    describe("Bicicleta.findByCode", () => {
        it("find bike by code", (done) => {
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);

                var b1 = new Bicicleta({ code: 1, color: "verde", modelo: "urbana" });
                Bicicleta.add(b1, function(err, newBike){
                    if (err) console.log(err);

                    var b2 = new Bicicleta({ code: 2, color: "roja", modelo: "urbana" });
                    Bicicleta.add(b2, function(err, newBike){
                        if (err) console.log(err);

                        Bicicleta.findByCode(1, function(error, targetBici){
                            expect(targetBici.code).toBe(b1.code);
                            expect(targetBici.color).toBe(b1.color);
                            expect(targetBici.modelo).toBe(b1.modelo);

                            done();
                        });
                    });
                });
    
            });
        });
    });

});












/* beforeEach(() => { Bicicleta.allBicis = []; });

describe ("Bicicleta.allBicis", () => {

    it("Empieza vacía", function() {
        expect(Bicicleta.allBicis.length).toBe(0);
      });
});



describe ("Bicicleta.add", () => {

    it("add one", function() {
        expect(Bicicleta.allBicis.length).toBe(0);
        var b = new Bicicleta(3,'azul', 'urbana', [6.2452124,-75.6511237]);
        Bicicleta.add(b);

        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(b);


      });
});

describe('Bicicleta.findById', () => {
    it('Debe devolver la bici con id 1', () => {
        expect(Bicicleta.allBicis.length).toBe(0);

        var b1 = new Bicicleta(1, "blanca", "urbana");
        var b2 = new Bicicleta(2, "verde", "montaña");

        Bicicleta.add(b1);
        Bicicleta.add(b2);

        var targetBici = Bicicleta.findById(1);
        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(b1.color);
        expect(targetBici.modelo).toBe(b1.modelo);
    });
}); */