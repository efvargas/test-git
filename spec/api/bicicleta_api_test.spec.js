var mongoose = require("mongoose");
var Bicicleta = require("../../models/bicicleta");
var request = require("request");
//var server = require("../../bin/www");




var base_url = "http://localhost:5000/api/bicicletas";

describe("Bicicleta API", () => {
    beforeAll(function(done){
       
        var mongoDB = "mongodb://localhost/testdb";
        mongoose.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true,  useCreateIndex: true});

        const db = mongoose.connection;

    
        db.on("error", console.error.bind(console, "Connection error!"));
        db.on("open", function(){
            console.log("We are connected to test database!");
            done();
        });
    });

    afterEach(function(done){
/*         Bicicleta.deleteMany({}, function(err, success){
            if (err) console.log(err);
            mongoose.connection.close();
            done();
        }); */
        
        done();
    });

  

    describe("POST CREATE", () => {
        it("Status 200", (done) => {
            var headers = { "content-type": "application/json" };
            var b = '{ "code": 4, "color": "azul", "modelo": "urbana", "lat": 6.2, "lng": -75.6 }';

            request.post({
                headers: headers,
                url: base_url + "/create",
                body: b
            }, function(error, response, body){
                expect(response.statusCode).toBe(200);
                var bici = JSON.parse(body).bicicleta;
                expect(bici.color).toBe("azul");
                expect(bici.ubicacion[0]).toBe(6.2);
                expect(bici.ubicacion[1]).toBe(-75.6);
                done();
            });
        });
    });

    describe("POST UPDATE", () => {
        it("Status 200", (done) => {
            var headers = { "content-type": "application/json" };
            var b = '{ "code": 4, "color": "negro", "modelo": "montana", "lat": 6.2, "lng": -75.6}';

            request.post({
                headers: headers,
                url: base_url + "/update",
                body: b
            }, function(error, response,body){
                expect(response.statusCode).toBe(200);
                var bici = JSON.parse(body).bicicleta;
                console.log(bici.color);
                expect(bici.color).toBe("negro");
                expect(bici.ubicacion[0]).toBe(6.2);
                expect(bici.ubicacion[1]).toBe(-75.6);
                done();
            });
        });
    });

    describe("GET ALL BICIS", () => {
        it("Status 200", (done) => {
            request.get(base_url, function(error, response, body){
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(1);
                done();
            });
        });
    }); 
        
     describe("POST DELETE", () => {
        it("Status 200", (done) => {
            var headers = { "content-type": "application/json" };
            var b = '{ "code": 4 }';

           

            request.delete({
                headers: headers,
                url: base_url + "/delete",
                body: b
            }, function(error, response,body){
                expect(response.statusCode).toBe(204);
                done();
            });
        });
    }); 

});
