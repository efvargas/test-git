var map = L.map('mapid').setView([4.6486259,-74.2478955], 6);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);


$.ajax({
    dataType:"json",
    url:"api/bicicletas",
    success:function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici) {
            L.marker(bici.ubicacion,{title:bici.id}).addTo(map);                        
        });
    }
})